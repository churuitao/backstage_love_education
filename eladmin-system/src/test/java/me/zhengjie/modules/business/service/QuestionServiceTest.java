package me.zhengjie.modules.business.service;

import me.zhengjie.AppRun;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes= AppRun.class)
public class QuestionServiceTest {

    @Autowired
    private QuestionService questionService;

}