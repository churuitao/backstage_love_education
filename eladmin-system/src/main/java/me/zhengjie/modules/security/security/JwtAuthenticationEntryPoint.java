package me.zhengjie.modules.security.security;

import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;

/**
 * 实现AuthenticationEntryPoint这个接口，发现没有凭证，往response中放些东西。
 * @author Zheng Jie
 */
@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint {

   /**
    * 功能描述： 凭证方法
    * @author RenShiWei
    * Date: 2020/2/9 12:19
    */
    @Override
    public void commence(HttpServletRequest request,
                         HttpServletResponse response,
                         AuthenticationException authException) throws IOException {
        // 当用户尝试访问安全的REST资源而不提供任何凭据时，将调用此方法发送401 响应
        response.sendError(HttpServletResponse.SC_UNAUTHORIZED, authException==null?"Unauthorized":authException.getMessage());
    }
}
