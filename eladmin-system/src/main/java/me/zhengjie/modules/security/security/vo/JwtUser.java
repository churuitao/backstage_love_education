package me.zhengjie.modules.security.security.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import java.sql.Timestamp;
import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

/**
 * 继承UserDetails，方便在UserDetailsServiceImpl中返回UserDetails对象
 * @author Zheng Jie
 * @date 2018-11-23
 */
@Getter
@AllArgsConstructor
public class JwtUser implements UserDetails {

    private final Long id;

    private final String username;

    private final String nickName;

    private final String sex;

    @JsonIgnore
    private final String password;

    private final String avatar;

    private final String email;

    private final String phone;

    private final String dept;

    private final String job;

    /**获取权限信息，存角色*/
    @JsonIgnore
    private final Collection<GrantedAuthority> authorities;

    private final boolean enabled;

    private Timestamp createTime;

    @JsonIgnore
    private final Date lastPasswordResetDate;

    /**账号是否未过期，默认是false，记得要改一下*/
    @JsonIgnore
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    /**账号是否未锁定，默认是false，记得也要改一下*/
    @JsonIgnore
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    /**账号凭证是否未过期，默认是false，记得还要改一下*/
    @JsonIgnore
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @JsonIgnore
    @Override
    public String getPassword() {
        return password;
    }

    /**这个有点抽象不会翻译，默认也是false，记得改一下*/
    @Override
    public boolean isEnabled() {
        return enabled;
    }

    public Collection getRoles() {
        return authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet());
    }
}
