package me.zhengjie.modules.security.security.vo;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class WxAuthUser {

    /** 用户的加密数据，可以通过解密来获取数据 */
    @NotBlank
    private String encryptedData;

    /** 加密开始向量 */
    @NotBlank
    private String iv;

    /** 用户类型 0 提问方 1 解答方 */
    @NotNull
    private Integer orientation;

    /** code */
    @NotBlank
    private String code;


}
