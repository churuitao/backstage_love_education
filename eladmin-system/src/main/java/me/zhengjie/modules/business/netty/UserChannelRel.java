package me.zhengjie.modules.business.netty;

import io.netty.channel.Channel;
import lombok.extern.slf4j.Slf4j;

import java.util.concurrent.ConcurrentHashMap;

/**
 * 功能描述：用户id和channel的关联关系的处理类
 *
 * @author RenShiWei
 * Date: 2020/3/12 21:51
 **/
@Slf4j
public class UserChannelRel {
    private static ConcurrentHashMap<Integer, Channel> manager = new ConcurrentHashMap<>();

    public static void put(Integer senderId,Channel channel) {
        manager.put(senderId,channel);
    }

    public static Channel get(Integer senderId) {
        return manager.get(senderId);
    }

    public static boolean isContainsKey(Integer userId){
        return manager.containsKey(userId);
    }

    public static void output() {
        manager.forEach(( key, value ) -> log.info("UserId:" + key + ",ChannelId:" +
                value.id().asLongText()));
    }

}

