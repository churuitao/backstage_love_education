package me.zhengjie.modules.business.netty.enums;

/**
 * 功能描述：客户端发送消息的动作类型
 *
 * @author RenShiWei
 * Date: 2020/3/9 20:57
 **/
public enum MsgActionEnum {
    /** 第一次(或重连)初始化连接 */
    CONNECT(1,"第一次(或重连)初始化连接"),
    /** 聊天消息 */
    CHAT(2,"聊天消息"),
    /** 消息签收 */
//    SIGNED(3,"消息签收"),
    /** 客户端保持心跳 */
    KEEPALIVE(3,"客户端保持心跳");

    public final Integer type;
    public final String content;

    private MsgActionEnum(Integer type,String content) {
        this.type = type;
        this.content = content;
    }
}