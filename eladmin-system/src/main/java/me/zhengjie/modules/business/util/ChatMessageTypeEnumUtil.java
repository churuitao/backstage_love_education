package me.zhengjie.modules.business.util;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * 枚举：聊天信息的类型
 * @author rsw
 */

@Getter
@NoArgsConstructor
@AllArgsConstructor
public enum ChatMessageTypeEnumUtil {
    /**文本*/
    TEXT("text"),
    /**图片*/
    IMAGE("image"),
    /**音频*/
    VOICE("voice"),
    ;
    private String chatType;

}
