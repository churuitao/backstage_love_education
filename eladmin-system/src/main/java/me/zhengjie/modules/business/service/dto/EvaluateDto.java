package me.zhengjie.modules.business.service.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @author crt
* @date 2020-02-02
*/
@Data
public class EvaluateDto implements Serializable {

    /** id */
    private Integer id;

    /** 问题 */
    private Integer questionId;

    /** 评价来源 */
    private Integer fromId;

    /** 评价目标 */
    private Integer toId;

    /** 评价内容 */
    private String context;

    /** 解答评价 */
    private Integer answerLevel;

    /** 条例清晰 */
    private Integer regulationLevel;

    /** 回复速度 */
    private Integer speedLevel;

    /** 态度评价 */
    private Integer attitudeLevel;

    /** 创建时间 */
    private Timestamp createTime;

    /** 更新时间 */
    private Timestamp updateTime;
}