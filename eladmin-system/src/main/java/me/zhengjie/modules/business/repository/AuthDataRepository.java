package me.zhengjie.modules.business.repository;

import me.zhengjie.modules.business.domain.AuthData;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
* @author churuitao
* @date 2020-02-02
*/
public interface AuthDataRepository extends JpaRepository<AuthData, Integer>, JpaSpecificationExecutor<AuthData> {



}