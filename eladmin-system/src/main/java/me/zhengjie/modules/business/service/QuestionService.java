package me.zhengjie.modules.business.service;

import me.zhengjie.modules.business.domain.Question;
import me.zhengjie.modules.business.service.dto.QuestionDto;
import me.zhengjie.modules.business.service.dto.QuestionQueryCriteria;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @author churuitao
* @date 2020-02-02
*/
public interface QuestionService {

    /**
    * 查询数据分页
    * @param criteria 条件
    * @param pageable 分页参数
    * @return Map<String,Object>
    */
    Map<String,Object> queryAll(QuestionQueryCriteria criteria, Pageable pageable);

    /**
    * 查询所有数据不分页
    * @param criteria 条件参数
    * @return List<QuestionDto>
    */
    List<QuestionDto> queryAll(QuestionQueryCriteria criteria);

    /**
     * 根据ID查询
     * @param id ID
     * @return QuestionDto
     */
    QuestionDto findById(Integer id);

    /**
    * 创建
    * @param resources /
    * @return QuestionDto
    */
    QuestionDto create(Question resources);

    /**
    * 编辑
    * @param resources /
    */
    void update(Question resources);

    /**
    * 多选删除
    * @param ids /
    */
    void deleteAll(Integer[] ids);

    /**
    * 导出数据
    * @param all 待导出的数据
    * @param response /
    * @throws IOException /
    */
    void download(List<QuestionDto> all, HttpServletResponse response) throws IOException;

    /**
     *
     * @description 根据条件汇总问题数量
     * @param id
     * @param type
     * @author churuitao
     * @date 2020年02月05日 10:35
     */
    Map countQuestion(Integer id, Integer type);
    /**
     * 根据问题id查询一条数据，携带提问方和解答方信息
     * @param questionId 问题id
     * @return QuestionDto
     */
    QuestionDto getQuestionById(Integer questionId);

    /**
     * 新增问题（问题图片上传）
     * @param resources 问题信息（实体类）
     * @param multipartFile 前台文件（图片）
     * @return 返回插入的信息
     */
    QuestionDto insertAndPicture(Question resources,MultipartFile multipartFile);

    /**
     *
     * description 用户抢答题
     * @param questionId 问题id
     * @param answerId 解答方id
     * @return
     * @author churuitao
     * @date 2020年02月05日 17:35
     */
    boolean rushThisQuestion(Integer questionId,Integer answerId);

    /**
     * 功能描述：解答方解答问题时（正在解答），调整问题状态为1
     * @param questionId 问题id
     * @param answerId 解答方id
     * @author RenShiWei
     * Date: 2020/2/17 22:51
     */
    void updateSolving(Integer questionId,Integer answerId );

    /**
     * 功能描述：解答完毕时，将聊天内容存进mysql，清空redis缓存
     * @param questionId 问题id
     * @param askId 提问方id
     * @param answerId 解答方id
     * @author RenShiWei
     * Date: 2020/2/7 15:49
     */
    void updateChatContent ( Integer questionId, Integer askId, Integer answerId );
    
    /**
     * 功能描述：查询历史聊天记录（从redis缓存中查询）
     * @param questionId 问题id
     * @param askId 提问方id
     * @param answerId 解答方id
     * @return 聊天信息的json字符串
     * @author RenShiWei
     * Date: 2020/2/7 18:51
     */
    String getChatContent ( Integer questionId, Integer askId, Integer answerId);

}