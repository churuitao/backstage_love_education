package me.zhengjie.modules.business.util;

import me.zhengjie.config.WxOfficialConfig;
import org.dom4j.Document;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import javax.servlet.http.HttpServletRequest;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 *  @title 微信工具包
 *  @Description 描述
 *  @author churuitao
 *  @Date 2020年01月27日 19:16
 *  @Version: 1.0
 *  @Copyright 2020-2021
 */
public class OfficialUtil {


    /**
     *
     * @description 验证身份
     * @param signature 微信加密签名
     * @param timestamp 时间戳
     * @return 返回验证结果
     * @author churuitao
     * @date 2020年01月27日 19:53
     */
    public static boolean validParams(String signature, String timestamp, String nonce){
        //创建数组
        String arr[] = new String[]{timestamp,nonce, WxOfficialConfig.TOKEN};

        //排序
        Arrays.sort(arr);

        StringBuilder sb = new StringBuilder();

        //生成字符串
        for(String a : arr){
            sb.append(a);
        }

        //sha1加密
        String formattedText = OfficialUtil.sha1(sb.toString());

        if(formattedText.equals(signature)){
            return true;
        }
        return false;
    }
    /**
     *
     * @description sha1加密
     * @param str 源字符串
     * @return 加密后的内容
     * @author churuitao
     * @date 2020年01月27日 19:25
     */
    public static String sha1(String str) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-1");
            digest.update(str.getBytes());
            byte messageDigest[] = digest.digest();
            // Create Hex String
            StringBuffer hexString = new StringBuffer();
            // 字节数组转换为 十六进制 数
            for (int i = 0; i < messageDigest.length; i++) {
                String shaHex = Integer.toHexString(messageDigest[i] & 0xFF);
                if (shaHex.length() < 2) {
                    hexString.append(0);
                }
                hexString.append(shaHex);
            }
            return hexString.toString();

        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return "";
    }

    /**
     *
     * @description 解析xml 返回 map
     * @param request 请求
     * @return map 结果
     * @author churuitao
     * @date 2020年01月27日 19:58
     */
    @SuppressWarnings({ "unchecked"})
    public static Map<String,String> parseXml(HttpServletRequest request) throws Exception {
        // 将解析结果存储在HashMap中
        Map<String,String> map = new HashMap<String,String>();
        // 从request中取得输入流
        InputStream inputStream = request.getInputStream();
        // 读取输入流
        SAXReader reader = new SAXReader();
        Document document = reader.read(inputStream);
        // 得到xml根元素
        Element root = document.getRootElement();
        // 得到根元素的所有子节点
        List<Element> elementList = root.elements();
        // 遍历所有子节点
        for (Element e : elementList)
            map.put(e.getName(), e.getText());
        // 释放资源
        inputStream.close();
        inputStream = null;
        return map;
    }
}
