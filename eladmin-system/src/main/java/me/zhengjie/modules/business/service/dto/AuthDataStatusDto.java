package me.zhengjie.modules.business.service.dto;

import lombok.Data;

import java.io.Serializable;

/**
* @author churuitao
* @date 2020-02-02
*/
@Data
public class AuthDataStatusDto implements Serializable {

    /** 认证状态 */
    private Integer status;

    /** 认证消息 */
    private String message;

}