package me.zhengjie.modules.business.service.mapper;

import me.zhengjie.base.BaseMapper;
import me.zhengjie.modules.business.domain.AuthData;
import me.zhengjie.modules.business.service.dto.AuthDataDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
* @author churuitao
* @date 2020-02-02
*/
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface AuthDataMapper extends BaseMapper<AuthDataDto, AuthData> {

}