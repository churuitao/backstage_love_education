package me.zhengjie.modules.business.service.mapper;

import me.zhengjie.base.BaseMapper;
import me.zhengjie.modules.business.domain.Evaluate;
import me.zhengjie.modules.business.service.dto.EvaluateDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
* @author crt
* @date 2020-02-02
*/
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface EvaluateMapper extends BaseMapper<EvaluateDto, Evaluate> {

}