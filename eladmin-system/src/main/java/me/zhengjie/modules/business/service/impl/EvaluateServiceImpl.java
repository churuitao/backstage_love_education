package me.zhengjie.modules.business.service.impl;

import me.zhengjie.modules.business.domain.Evaluate;
import me.zhengjie.utils.ValidationUtil;
import me.zhengjie.utils.FileUtil;
import me.zhengjie.modules.business.repository.EvaluateRepository;
import me.zhengjie.modules.business.service.EvaluateService;
import me.zhengjie.modules.business.service.dto.EvaluateDto;
import me.zhengjie.modules.business.service.dto.EvaluateQueryCriteria;
import me.zhengjie.modules.business.service.mapper.EvaluateMapper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
// 默认不使用缓存
//import org.springframework.cache.annotation.CacheConfig;
//import org.springframework.cache.annotation.CacheEvict;
//import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import me.zhengjie.utils.PageUtil;
import me.zhengjie.utils.QueryHelp;
import java.util.List;
import java.util.Map;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
* @author crt
* @date 2020-02-02
*/
@Service
//@CacheConfig(cacheNames = "evaluate")
@Transactional(propagation = Propagation.SUPPORTS, readOnly = true, rollbackFor = Exception.class)
public class EvaluateServiceImpl implements EvaluateService {

    private final EvaluateRepository evaluateRepository;

    private final EvaluateMapper evaluateMapper;

    public EvaluateServiceImpl(EvaluateRepository evaluateRepository, EvaluateMapper evaluateMapper) {
        this.evaluateRepository = evaluateRepository;
        this.evaluateMapper = evaluateMapper;
    }

    @Override
    //@Cacheable
    public Map<String,Object> queryAll(EvaluateQueryCriteria criteria, Pageable pageable){
        Page<Evaluate> page = evaluateRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder),pageable);
        return PageUtil.toPage(page.map(evaluateMapper::toDto));
    }

    @Override
    //@Cacheable
    public List<EvaluateDto> queryAll(EvaluateQueryCriteria criteria){
        return evaluateMapper.toDto(evaluateRepository.findAll((root, criteriaQuery, criteriaBuilder) -> QueryHelp.getPredicate(root,criteria,criteriaBuilder)));
    }

    @Override
    //@Cacheable(key = "#p0")
    public EvaluateDto findById(Integer id) {
        Evaluate evaluate = evaluateRepository.findById(id).orElseGet(Evaluate::new);
        ValidationUtil.isNull(evaluate.getId(),"Evaluate","id",id);
        return evaluateMapper.toDto(evaluate);
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public EvaluateDto create(Evaluate resources) {
        return evaluateMapper.toDto(evaluateRepository.save(resources));
    }

    @Override
    //@CacheEvict(allEntries = true)
    @Transactional(rollbackFor = Exception.class)
    public void update(Evaluate resources) {
        Evaluate evaluate = evaluateRepository.findById(resources.getId()).orElseGet(Evaluate::new);
        ValidationUtil.isNull( evaluate.getId(),"Evaluate","id",resources.getId());
        evaluate.copy(resources);
        evaluateRepository.save(evaluate);
    }

    @Override
    //@CacheEvict(allEntries = true)
    public void deleteAll(Integer[] ids) {
        for (Integer id : ids) {
            evaluateRepository.deleteById(id);
        }
    }

    @Override
    public void download(List<EvaluateDto> all, HttpServletResponse response) throws IOException {
        List<Map<String, Object>> list = new ArrayList<>();
        for (EvaluateDto evaluate : all) {
            Map<String,Object> map = new LinkedHashMap<>();
            map.put("问题", evaluate.getQuestionId());
            map.put("评价来源", evaluate.getFromId());
            map.put("评价目标", evaluate.getToId());
            map.put("评价内容", evaluate.getContext());
            map.put("解答评价", evaluate.getAnswerLevel());
            map.put("条例清晰", evaluate.getRegulationLevel());
            map.put("回复速度", evaluate.getSpeedLevel());
            map.put("态度评价", evaluate.getAttitudeLevel());
            map.put("创建时间", evaluate.getCreateTime());
            map.put("更新时间", evaluate.getUpdateTime());
            list.add(map);
        }
        FileUtil.downloadExcel(list, response);
    }
}