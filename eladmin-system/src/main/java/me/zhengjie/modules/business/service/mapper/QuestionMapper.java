package me.zhengjie.modules.business.service.mapper;

import me.zhengjie.base.BaseMapper;
import me.zhengjie.modules.business.domain.Question;
import me.zhengjie.modules.business.domain.WxUser;
import me.zhengjie.modules.business.service.dto.QuestionDto;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Mappings;
import org.mapstruct.ReportingPolicy;

/**
* @author churuitao
* @date 2020-02-02
*/
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface QuestionMapper extends BaseMapper<QuestionDto, Question> {

    /**
     * 查询一条问题信息
     * @param question 问题试题
     * @param askUser   提问方
     * @param answerUser 解答方
     * @return  QuestionDto
     */
    @Mappings({
            @Mapping(source = "question.id",target = "id"),
            @Mapping(source = "question.askId",target = "askId"),
            @Mapping(source = "question.answerId",target = "answerId"),
            @Mapping(source = "question.picturePath",target = "picturePath"),
            @Mapping(source = "question.thumbnailPath",target = "thumbnailPath"),
            @Mapping(source = "question.status",target = "status"),
            @Mapping(source = "question.hidden",target = "hidden"),
            @Mapping(source = "question.createTime",target = "createTime"),
            @Mapping(source = "question.title",target = "title"),
            @Mapping(source = "question.questionType",target = "questionType"),
            @Mapping(source = "question.context",target = "context"),
            @Mapping(source = "question.updateTime",target = "updateTime"),
            @Mapping(source = "question.solveTime",target = "solveTime"),
            @Mapping(source = "question.beginAnswerTime",target = "beginAnswerTime"),
            @Mapping(source = "askUser.nickName",target = "askNickName"),
            @Mapping(source = "askUser.avatar",target = "askAvatar"),
            @Mapping(source = "answerUser.nickName",target = "answerNickName"),
            @Mapping(source = "answerUser.avatar",target = "answerAvatar"),
    })
    QuestionDto from(Question question, WxUser askUser, WxUser answerUser);
}