package me.zhengjie.modules.business.repository;

import me.zhengjie.modules.business.domain.Question;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
* @author churuitao
* @date 2020-02-02
*/
public interface QuestionRepository extends JpaRepository<Question, Integer>, JpaSpecificationExecutor<Question> {

    Long countByAskId(Integer askId);
    Long countByAskIdAndStatus(Integer askId,Integer status);

    Long countByAnswerId(Integer answerId);
    Long countByAnswerIdAndStatus(Integer askId,Integer status);


}