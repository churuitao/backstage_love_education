package me.zhengjie.modules.business.service.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @author churuitao
* @date 2020-02-04
*/
@Data
public class WxUserDto implements Serializable {

    /** id */
    private Integer id;

    /** 头像 */
    private String avatar;

    /** 邮箱 */
    private String email;

    /** 状态 */
    private Boolean enabled;

    /** 昵称 */
    private String nickName;

    /** 性别 */
    private String sex;

    /** 微信小程序id */
    private String miniOpenId;

    /** 微信公众号id */
    private String officialOpenId;

    /** 微信唯一标识 */
    private String unionId;

    /** 所在地 */
    private String location;

    /** 用户定位 */
    private Integer orientation;
//
//    /** 认证资料id */
//    private Integer authDataId;

    /** 创建时间 */
    private Timestamp createTime;

    /** 更新时间 */
    private Timestamp updateTime;

    /** 认证资料 */
    private AuthDataDto authData;
}