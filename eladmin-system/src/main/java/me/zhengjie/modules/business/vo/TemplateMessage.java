package me.zhengjie.modules.business.vo;

import lombok.Data;

import java.util.Map;

@Data
public class TemplateMessage {

    /** 接收方微信公众号openid */
    private String touser;

    /** 模板id */
    private String template_id;

    /** 模板消息跳转路径 */
    private String url;

    /** top颜色 */
    private String topcolor;

    /** 小程序相关设置 */
    private Map<String, String> miniprogram;

    /** 模板数据 */
    private Map<String ,TemplateData> data;

}
