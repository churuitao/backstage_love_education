package me.zhengjie.modules.business.service.mapper;

import me.zhengjie.base.BaseMapper;
import me.zhengjie.modules.business.domain.WxUser;
import me.zhengjie.modules.business.service.dto.WxUserDto;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

/**
* @author churuitao
* @date 2020-02-04
*/
@Mapper(componentModel = "spring", unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface WxUserMapper extends BaseMapper<WxUserDto, WxUser> {

}