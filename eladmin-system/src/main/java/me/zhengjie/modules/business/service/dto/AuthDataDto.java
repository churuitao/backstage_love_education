package me.zhengjie.modules.business.service.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @author churuitao
* @date 2020-02-02
*/
@Data
public class AuthDataDto implements Serializable {

    /** id */
    private Integer id;

    /** 姓名 */
    private String realName;

    /** 性别 */
    private String sex;

    /** 出生日期 */
    private Timestamp birthData;

    /** 身份证号 */
    private String idCard;

    /** 学校 */
    private String school;

    /** 班级 */
    private String classes;

    /** 专业 */
    private String major;

    /** 学号 */
    private String stuNo;

    /** 解答科目 */
    private String answerSubject;

    /** 认证状态 */
    private Integer status;

    /** 认证消息 */
    private String message;

    /** 创建时间 */
    private Timestamp createTime;

    /** 更新时间 */
    private Timestamp updateTime;
}