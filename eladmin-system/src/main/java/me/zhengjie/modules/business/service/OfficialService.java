package me.zhengjie.modules.business.service;

import me.zhengjie.modules.business.message.BaseMessage;

import java.util.Map;

public interface OfficialService {

    /**
     * description 解析请求方法，并返回数据
     * @param requestMap
     * @return
     * @author churuitao
     * @date 2020年02月10日 10:04
     */
    String parseMessage(Map<String, String> requestMap);

    /**
     *
     * @description 处理文字消息
     * @param requestMap 参数
     * @return
     * @author churuitao
     * @date 2020年01月27日 21:10
     */
    BaseMessage handelTextRequest(Map<String, String> requestMap);

    /**
     *
     * @description Bean 变成 xml
     * @param msg 消息类
     * @return
     * @author churuitao
     * @date 2020年01月27日 23:38
     */
    String beanTOXml(BaseMessage msg);

    /**
     * description 获取token
     * @author churuitao
     * date 2020年02月10日 10:29
     */
    String getAccessToken();
}
