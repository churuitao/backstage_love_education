package me.zhengjie.modules.business.service;

import org.springframework.web.multipart.MultipartFile;

import java.util.Map;

/**
 * class description：关于文件的业务处理
 *
 * @author rsw
 * Date: 2020/2/5
 * Time: 14:25
 **/
public interface FileService {


    /**
     * 上传文件，并返回文件上传地址
     *
     * @param multipartFile 前台传来的文件
     * @return 文件上传地址
     */
    String upload ( MultipartFile multipartFile );

    /**
     * 上传图片，并返回文件图片地址
     *
     * @param multipartFile 前台传来的图片
     * @return 图片上传地址
     */
    Map<String,String> uploadImage ( MultipartFile multipartFile );


}

