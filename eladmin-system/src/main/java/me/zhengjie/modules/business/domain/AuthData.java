package me.zhengjie.modules.business.domain;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.sql.Timestamp;

/**
* @author churuitao
* @date 2020-02-02
*/
@Entity
@Data
@Table(name="auth_data")
public class AuthData implements Serializable {


    /** id */
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    /** 姓名 */
    @Column(name = "real_name",nullable = false)
    @NotBlank
    private String realName;

    /** 性别 */
    @Column(name = "sex",nullable = false)
    @NotBlank
    private String sex;

    /** 出生日期 */
    @Column(name = "birth_data",nullable = false)
    @NotNull
    private Timestamp birthData;

    /** 身份证号 */
    @Column(name = "id_card",nullable = false)
    @NotBlank
    private String idCard;

    /** 学校 */
    @Column(name = "school",nullable = false)
    @NotBlank
    private String school;

    /** 班级 */
    @Column(name = "classes",nullable = false)
    @NotBlank
    private String classes;

    /** 专业 */
    @Column(name = "major",nullable = false)
    private String major;

    /** 学号 */
    @Column(name = "stu_no",nullable = false)
    private String stuNo;

    /** 解答科目 */
    @Column(name = "answer_subject",nullable = false)
    private String answerSubject;

    /** 认证状态 */
    @Column(name = "status")
    private Integer status;

    /** 认证消息 */
    @Column(name = "message")
    private String message;

    /** 创建时间 */
    @Column(name = "create_time")
    @CreationTimestamp
    private Timestamp createTime;

    /** 更新时间 */
    @Column(name = "update_time")
    @UpdateTimestamp
    private Timestamp updateTime;


    public void copy(AuthData source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}