package me.zhengjie.modules.business.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhengjie.annotation.AnonymousAccess;
import me.zhengjie.aop.log.Log;
import me.zhengjie.modules.business.domain.WxUser;
import me.zhengjie.modules.business.service.WxUserService;
import me.zhengjie.modules.business.service.dto.WxUserQueryCriteria;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
* @author churuitao
* @date 2020-02-04
*/
@Api(tags = "业务：微信用户管理")
@RestController
@RequestMapping("/api/wxUser")
public class WxUserController {

    private final WxUserService wxUserService;

    public WxUserController(WxUserService wxUserService) {
        this.wxUserService = wxUserService;
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    //@PreAuthorize("@el.check('wxUser:list')")
    @AnonymousAccess
    public void download(HttpServletResponse response, WxUserQueryCriteria criteria) throws IOException {
        wxUserService.download(wxUserService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询微信用户")
    @ApiOperation("查询微信用户")
    //@PreAuthorize("@el.check('wxUser:list')")
    @AnonymousAccess
    public ResponseEntity<Object> getWxUsers(WxUserQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(wxUserService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增微信用户")
    @ApiOperation("新增微信用户")
    //@PreAuthorize("@el.check('wxUser:add')")
    @AnonymousAccess
    public ResponseEntity<Object> create(@Validated @RequestBody WxUser resources){
        return new ResponseEntity<>(wxUserService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改微信用户")
    @ApiOperation("修改微信用户")
    //@PreAuthorize("@el.check('wxUser:edit')")
    @AnonymousAccess
    public ResponseEntity<Object> update(@Validated @RequestBody WxUser resources){
        wxUserService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除微信用户")
    @ApiOperation("删除微信用户")
    //@PreAuthorize("@el.check('wxUser:del')")
    @AnonymousAccess
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody Integer[] ids) {
        wxUserService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("find")
    @Log("根据Id查询微信用户")
    @ApiOperation("根据Id查询微信用户")
    @AnonymousAccess
    public ResponseEntity<Object> findById(Integer id){
        return new ResponseEntity<>(wxUserService.findById(id),HttpStatus.OK);
    }



}