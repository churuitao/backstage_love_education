package me.zhengjie.modules.business.netty.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 功能描述：消息签收的枚举类
 *
 * @author RenShiWei
 * Date: 2020/3/13 12:15
 **/
public enum MsgSignFlagEnum {
    /** 消息是否签收 */
    unsign(0,"未签收"),
    signed(1,"已签收");

    @Getter
    public final int type;
    @Getter
    public final String value;

    private MsgSignFlagEnum(int type,String value) {
        this.type = type;
        this.value = value;
    }


}
