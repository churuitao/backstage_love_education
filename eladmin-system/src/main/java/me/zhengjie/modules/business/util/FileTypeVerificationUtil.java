package me.zhengjie.modules.business.util;

import cn.hutool.core.io.FileTypeUtil;
import me.zhengjie.utils.FileUtil;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * class description：文件类型验证工具——基于Hutool工具包
 *
 * @author rsw
 * Date: 2020/2/5
 * Time: 15:30
 **/
public class FileTypeVerificationUtil {


    public static boolean verifyImageFile(MultipartFile multipartFile){
        BufferedImage bi = null;
        try {
            bi = ImageIO.read(multipartFile.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return !(bi == null);
    }

    /**
     * 判断文件类型是否正确
     *
     * @param multipartFile 前端文件
     * @param fileTypeArr   预判断文件类型集合
     * @return boolean
     */
    public static boolean verifyFileType(MultipartFile multipartFile, String[] fileTypeArr) {
        File file = FileUtil.toFile(multipartFile);
        for (String imageType : fileTypeArr) {
            if (imageType.equals(FileTypeUtil.getType(file))) {
                FileUtil.toMultipartFile(file);
                return true;
            }
        }
        return false;
    }

    /**
     * @param file        文件
     * @param fileTypeArr 预判断类型
     * @return boolean
     */
    public static boolean verifyFileType(File file, String[] fileTypeArr) {
        for (String imageType : fileTypeArr) {
            if (imageType.equals(FileTypeUtil.getType(file))) {
                return true;
            }
        }
        return false;
    }



}

