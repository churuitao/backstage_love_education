package me.zhengjie.modules.business.domain;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;


/**
* @author churuitao
* @date 2020-02-02
*/
@Entity
@Data
@Table(name="question")
public class Question implements Serializable {

    /** id */
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    /** 提问方 */
    @Column(name = "ask_id",nullable = false)
//    @NotNull
    private Integer askId;

    /** 解答方 */
    @Column(name = "answer_id",nullable = false)
//    @NotNull
    private Integer answerId;

    /** 图片路径 */
    @Column(name = "picture_path",nullable = false)
//    @NotBlank
    private String picturePath;

    /** 缩略图路径 */
    @Column(name = "thumbnail_path",nullable = false)
//    @NotBlank
    private String thumbnailPath;

    /** 问题状态 */
    @Column(name = "status",nullable = false)
//    @NotNull
    private Integer status;

    /** 隐藏 */
    @Column(name = "hidden",nullable = false)
//    @NotNull
    private Boolean hidden;

    /** 创建日期(提问日期) */
    @Column(name = "create_time")
    @CreationTimestamp
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Timestamp createTime;

    /** 问题标题 */
    @Column(name = "title",nullable = false)
//    @NotBlank
    private String title;

    /** 问题类型 */
    @Column(name = "question_type",nullable = false)
//    @NotBlank
    private String questionType;

    /** 聊天内容 */
    @Column(name = "context",nullable = false)
//    @NotBlank
    private String context;

    /** 更新时间 */
    @Column(name = "update_time")
    @UpdateTimestamp
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Timestamp updateTime;

    /** 解决时间 */
    @Column(name = "solve_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Timestamp solveTime;

    /** 开始解答时间 */
    @Column(name = "begin_answer_time")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
    private Timestamp beginAnswerTime;


    public void copy(Question source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }



}