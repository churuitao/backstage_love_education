package me.zhengjie.modules.business.service.dto;

import lombok.Data;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @author churuitao
* @date 2020-02-02
*/
@Data
public class QuestionDto implements Serializable {

    /** id */
    private Integer id;

    /** 提问方 */
    private Integer askId;

    /** 解答方 */
    private Integer answerId;

    /** 图片路径 */
    private String picturePath;

    /** 缩略图路径 */
    private String thumbnailPath;

    /** 问题状态 */
    private Integer status;

    /** 隐藏 */
    private Boolean hidden;

    /** 创建日期 */
    private Timestamp createTime;

    /** 问题标题 */
    private String title;

    /** 问题类型 */
    private String questionType;

    /** 聊天内容 */
    private String context;

    /** 解答时间 */
    private Timestamp updateTime;

    /** 解决时间 */
    private Timestamp solveTime;

    /** 开始解答时间 */
    private Timestamp beginAnswerTime;

    /** 提问方昵称 */
    private String askNickName;

    /**提问方头像*/
    private String askAvatar;

    /** 解答方昵称 */
    private String answerNickName;

    /**解答方头像*/
    private String answerAvatar;

}