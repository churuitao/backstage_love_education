package me.zhengjie.modules.business.domain;

import lombok.Data;
import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import javax.persistence.*;
import javax.validation.constraints.*;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.hibernate.annotations.*;
import java.sql.Timestamp;
import java.io.Serializable;

/**
* @author crt
* @date 2020-02-02
*/
@Entity
@Data
@Table(name="evaluate")
public class Evaluate implements Serializable {

    /** id */
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Integer id;

    /** 问题 */
    @Column(name = "question_id",nullable = false)
    @NotNull
    private Integer questionId;

    /** 评价来源 */
    @Column(name = "from_id",nullable = false)
    @NotNull
    private Integer fromId;

    /** 评价目标 */
    @Column(name = "to_id",nullable = false)
    @NotNull
    private Integer toId;

    /** 评价内容 */
    @Column(name = "context",nullable = false)
    @NotBlank
    private String context;

    /** 解答评价 */
    @Column(name = "answer_level",nullable = false)
    @NotNull
    private Integer answerLevel;

    /** 条例清晰 */
    @Column(name = "regulation_level",nullable = false)
    @NotNull
    private Integer regulationLevel;

    /** 回复速度 */
    @Column(name = "speed_level",nullable = false)
    @NotNull
    private Integer speedLevel;

    /** 态度评价 */
    @Column(name = "attitude_level",nullable = false)
    @NotNull
    private Integer attitudeLevel;

    /** 创建时间 */
    @Column(name = "create_time")
    @CreationTimestamp
    private Timestamp createTime;

    /** 更新时间 */
    @Column(name = "update_time")
    @UpdateTimestamp
    private Timestamp updateTime;

    public void copy(Evaluate source){
        BeanUtil.copyProperties(source,this, CopyOptions.create().setIgnoreNullValue(true));
    }
}