package me.zhengjie.modules.business.rest;

import io.swagger.models.auth.In;
import me.zhengjie.annotation.AnonymousAccess;
import me.zhengjie.aop.log.Log;
import me.zhengjie.modules.business.domain.Evaluate;
import me.zhengjie.modules.business.service.EvaluateService;
import me.zhengjie.modules.business.service.dto.EvaluateQueryCriteria;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import io.swagger.annotations.*;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @author crt
* @date 2020-02-02
*/
@Api(tags = "业务：评价管理")
@RestController
@RequestMapping("/api/evaluate")
public class EvaluateController {

    private final EvaluateService evaluateService;

    public EvaluateController(EvaluateService evaluateService) {
        this.evaluateService = evaluateService;
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('evaluate:list')")
    public void download(HttpServletResponse response, EvaluateQueryCriteria criteria) throws IOException {
        evaluateService.download(evaluateService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("查询evalaute")
    @ApiOperation("查询evalaute")
    //@PreAuthorize("@el.check('evaluate:list')")
    @AnonymousAccess
    public ResponseEntity<Object> getEvaluates(EvaluateQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(evaluateService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增evalaute")
    @ApiOperation("新增evalaute")
    //@PreAuthorize("@el.check('evaluate:add')")
    @AnonymousAccess
    public ResponseEntity<Object> create(@Validated @RequestBody Evaluate resources){
        return new ResponseEntity<>(evaluateService.create(resources),HttpStatus.CREATED);
    }

    @PutMapping
    @Log("修改evalaute")
    @ApiOperation("修改evalaute")
    //@PreAuthorize("@el.check('evaluate:edit')")
    @AnonymousAccess
    public ResponseEntity<Object> update(@Validated @RequestBody Evaluate resources){
        evaluateService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除evalaute")
    @ApiOperation("删除evalaute")
    //@PreAuthorize("@el.check('evaluate:del')")
    @AnonymousAccess
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody Integer[] ids) {
        evaluateService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}