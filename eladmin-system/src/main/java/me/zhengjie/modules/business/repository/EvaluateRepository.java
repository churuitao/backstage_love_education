package me.zhengjie.modules.business.repository;

import com.qiniu.util.Auth;
import me.zhengjie.modules.business.domain.AuthData;
import me.zhengjie.modules.business.domain.Evaluate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

/**
* @author crt
* @date 2020-02-02
*/
public interface EvaluateRepository extends JpaRepository<Evaluate, Integer>, JpaSpecificationExecutor<Evaluate> {

}