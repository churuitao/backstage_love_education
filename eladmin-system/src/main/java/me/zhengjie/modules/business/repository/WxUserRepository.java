package me.zhengjie.modules.business.repository;

import me.zhengjie.modules.business.domain.WxUser;
import org.apache.poi.ss.formula.functions.T;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

import java.util.Optional;

/**
* @author churuitao
* @date 2020-02-04
*/
public interface WxUserRepository extends JpaRepository<WxUser, Integer>, JpaSpecificationExecutor<WxUser> {

    /**
     *
     * @description 根据微信小程序openid获取用户信息
     * @param miniOpenId
     * @return
     * @author churuitao
     * @date 2020年02月06日 15:26
     */
    WxUser findByMiniOpenId(String miniOpenId);

}