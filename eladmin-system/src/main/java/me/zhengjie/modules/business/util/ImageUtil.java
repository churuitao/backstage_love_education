package me.zhengjie.modules.business.util;

import cn.hutool.core.io.FileTypeUtil;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

/**
 * class description：文件类型验证工具——基于Hutool工具包
 *
 * @author rsw
 * Date: 2020/2/5
 * Time: 15:30
 **/
public class ImageUtil {


    /**
     * 验证图片
     */
    public static boolean verifyImageFile ( MultipartFile multipartFile ) {
        BufferedImage bi = null;
        try {
            bi = ImageIO.read(multipartFile.getInputStream());
        } catch (IOException e) {
            e.printStackTrace();
        }
        return !(bi == null);
    }

    /**
     * 判断文件类型是否正确
     *
     * @param multipartFile 前端文件
     * @param fileTypeArr   预判断文件类型集合
     * @return boolean
     */
    public static boolean verifyFileType ( MultipartFile multipartFile, String[] fileTypeArr ) {
        File file = me.zhengjie.utils.FileUtil.toFile(multipartFile);
        for (String imageType : fileTypeArr) {
            if (imageType.equals(FileTypeUtil.getType(file))) {
                me.zhengjie.utils.FileUtil.toMultipartFile(file);
                return true;
            }
        }
        return false;
    }

    /**
     * @param file        文件
     * @param fileTypeArr 预判断类型
     * @return boolean
     */
    public static boolean verifyFileType ( File file, String[] fileTypeArr ) {
        for (String imageType : fileTypeArr) {
            if (imageType.equals(FileTypeUtil.getType(file))) {
                return true;
            }
        }
        return false;
    }

    /**
     * 功能描述：图片按比例压缩上传
     * @param file 文件路径
     * @param scale 压缩比例
     * @param outFilepath 输出文件目录
     * @author RenShiWei
     * Date: 2020/2/14 11:56
     */
    public static void imageScaleUpload ( String file, double scale, String outFilepath ){
        try {
            Thumbnails.of(file)
                    // 图片缩放率，不能和size()一起使用
                    .scale(scale)
                    // 缩略图保存目录,该目录需存在，否则报错
                    .toFile(outFilepath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * 功能描述：图片按大小压缩上传
     * @param file 文件路径
     * @param scaleWith 压缩宽
     * @param scaleHeight 压缩高
     * @param outFilepath 输出文件目录
     * @author RenShiWei
     * Date: 2020/2/14 11:56
     */
    public static void imageScaleUpload ( String file, int scaleWith,int scaleHeight, String outFilepath ){
        try {
            Thumbnails.of(file)
                    // 图片缩放率，不能和size()一起使用
                    .size(scaleWith, scaleHeight)
                    // 缩略图保存目录,该目录需存在，否则报错
                    .toFile(outFilepath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}

