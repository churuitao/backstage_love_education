package me.zhengjie.modules.business.service.dto;

import lombok.Data;
import me.zhengjie.annotation.Query;

/**
* @author churuitao
* @date 2020-02-02
*/
@Data
public class QuestionQueryCriteria{

    /**
     * 设置查询条件为等于
     */
    @Query
    private Integer status;

    @Query
    private Integer askId;

    @Query
    private Integer answerId;

    @Query
    private Boolean hidden;
}