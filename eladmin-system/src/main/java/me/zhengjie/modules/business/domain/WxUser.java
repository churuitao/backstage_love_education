package me.zhengjie.modules.business.domain;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.bean.copier.CopyOptions;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import javax.persistence.*;
import java.io.Serializable;
import java.sql.Timestamp;

/**
* @author churuitao
* @date 2020-02-04
*/
@Entity
@Data
@Table(name="wx_user")
public class WxUser implements Serializable {

    /**
     * id
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;

    /**
     * 头像
     */
    @Column(name = "avatar")
    private String avatar;


    /**
     * 邮箱
     */
    @Column(name = "email")
    private String email;

    /**
     * 状态
     */
    @Column(name = "enabled")
    private Boolean enabled;

    /**
     * 昵称
     */
    @Column(name = "nick_name")
    private String nickName;

    /**
     * 性别
     */
    @Column(name = "sex")
    private String sex;

    /**
     * 微信小程序id
     */
    @Column(name = "mini_open_id")
    private String miniOpenId;

    /**
     * 微信公众号id
     */
    @Column(name = "official_open_id")
    private String officialOpenId;

    /**
     * 微信唯一标识
     */
    @Column(name = "union_id")
    private String unionId;

    /**
     * 所在地
     */
    @Column(name = "location")
    private String location;

    /**
     * 用户定位
     */
    @Column(name = "orientation")
    private Integer orientation;

    /**
     * 认证资料id
     */
    @Column(name = "auth_data_id")
    @Transient
    private Integer authDataId;

    /**
     * 创建时间
     */
    @Column(name = "create_time")
    @CreationTimestamp
    private Timestamp createTime;

    /**
     * 更新时间
     */
    @Column(name = "update_time")
    @UpdateTimestamp
    private Timestamp updateTime;

    public void copy(WxUser source) {
        BeanUtil.copyProperties(source, this, CopyOptions.create().setIgnoreNullValue(true));
    }

    @OneToOne(targetEntity = AuthData.class)
    @JoinColumn(name = "auth_data_id", unique = true)
    private AuthData authData;

}