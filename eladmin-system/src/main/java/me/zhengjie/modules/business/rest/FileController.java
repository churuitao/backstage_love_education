package me.zhengjie.modules.business.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhengjie.annotation.AnonymousAccess;
import me.zhengjie.aop.log.Log;
import me.zhengjie.modules.business.domain.Question;
import me.zhengjie.modules.business.service.FileService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

/**
 * class description：关于文件的Controller类
 *
 * @author rsw
 * Date: 2020/2/5
 * Time: 14:24
 **/
@Api(tags = "业务：文件管理")
@RestController
@RequestMapping("/api/file")
public class FileController {

    @Autowired
    private FileService fileService;

    @ApiOperation("文件上传（任意类型文件）")
    @Log("文件上传（任意类型文件）")
    @PostMapping(value = "/upload-file")
    @AnonymousAccess
    public ResponseEntity<Object> uploadFile(@RequestPart MultipartFile file){
        return new ResponseEntity<>(fileService.upload(file), HttpStatus.OK);
    }

    @ApiOperation("文件上传（问题图片上传）")
    @Log("文件上传（问题图片上传）")
    @PostMapping(value = "/upload-image")
    @AnonymousAccess
    public ResponseEntity<Object> insertWithPicture(@RequestPart MultipartFile file){
        return new ResponseEntity<>(fileService.uploadImage(file), HttpStatus.OK);
    }

}

