package me.zhengjie.modules.business.vo;

import lombok.Data;

@Data
public class TemplateData {

    /** 值 */
    private String value;

    /** 颜色 */
    private String color;

    public TemplateData(String value, String color) {
        this.value = value;
        this.color = color;
    }
}
