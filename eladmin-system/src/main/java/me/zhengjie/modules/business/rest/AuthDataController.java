package me.zhengjie.modules.business.rest;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import me.zhengjie.annotation.AnonymousAccess;
import me.zhengjie.aop.log.Log;
import me.zhengjie.modules.business.domain.AuthData;
import me.zhengjie.modules.business.service.AuthDataService;
import me.zhengjie.modules.business.service.WxUserService;
import me.zhengjie.modules.business.service.dto.AuthDataQueryCriteria;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
* @author churuitao
* @date 2020-02-02
*/
@Api(tags = "业务：认证管理")
@RestController
@RequestMapping("/api/authData")
public class AuthDataController {

    private final AuthDataService authDataService;

    public AuthDataController(AuthDataService authDataService, WxUserService wxUserService) {
        this.authDataService = authDataService;
    }

    @Log("导出数据")
    @ApiOperation("导出数据")
    @GetMapping(value = "/download")
    @PreAuthorize("@el.check('authData:list')")
    public void download(HttpServletResponse response, AuthDataQueryCriteria criteria) throws IOException {
        authDataService.download(authDataService.queryAll(criteria), response);
    }

    @GetMapping
    @Log("分页查询认证管理")
    @ApiOperation("分页查询认证管理")
    //@PreAuthorize("@el.check('authData:list')")
    @AnonymousAccess
    public ResponseEntity<Object> getAuthData(AuthDataQueryCriteria criteria, Pageable pageable){
        return new ResponseEntity<>(authDataService.queryAll(criteria,pageable),HttpStatus.OK);
    }

    @PostMapping
    @Log("新增认证信息")
    @ApiOperation("新增认证信息")
    //@PreAuthorize("@el.check('authData:add')")
    @AnonymousAccess
    public ResponseEntity<Object> create(@Validated @RequestBody AuthData resources){
        return new ResponseEntity<>(authDataService.create(resources),HttpStatus.CREATED);
    }


    @PutMapping
    @Log("修改认证信息")
    @ApiOperation("修改认证信息")
    //@PreAuthorize("@el.check('authData:edit')")
    @AnonymousAccess
    public ResponseEntity<Object> update(@Validated @RequestBody AuthData resources){
        authDataService.update(resources);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @Log("删除认证信息")
    @ApiOperation("删除认证信息")
    //@PreAuthorize("@el.check('authData:del')")
    @AnonymousAccess
    @DeleteMapping
    public ResponseEntity<Object> deleteAll(@RequestBody Integer[] ids) {
        authDataService.deleteAll(ids);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("find")
    @Log("根据用户ID查询认证信息")
    @ApiOperation("根据用户ID查询认证信息")
    //@PreAuthorize("@el.check('authData:list')")
    @AnonymousAccess
    public ResponseEntity<Object> findById(Integer id){
        return new ResponseEntity<>(authDataService.findByUserId(id), HttpStatus.OK);
    }

    @GetMapping("inquiry")
    @Log("根据用户ID查询认证状态")
    @ApiOperation("根据用户ID查询认证状态")
    //@PreAuthorize("@el.check('authData:list')")
    @AnonymousAccess
    public ResponseEntity<Object> inquiryById(Integer id){
        return new ResponseEntity<>(authDataService.inquiryById(id), HttpStatus.OK);
    }

    @PostMapping("/put/{id}")
    @Log("新增修改用户关联认证数据")
    @ApiOperation("新增修改用户关联认证数据")
    //@PreAuthorize("@el.check('evaluate:add')")
    @AnonymousAccess
    public ResponseEntity<Object> createOrUpdateByUser(@Validated @RequestBody AuthData resources, @PathVariable Integer id){
        authDataService.createOrUpdateByUserId(resources,id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

}
