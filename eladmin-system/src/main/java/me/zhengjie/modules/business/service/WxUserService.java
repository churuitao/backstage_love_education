package me.zhengjie.modules.business.service;

import me.zhengjie.modules.business.domain.AuthData;
import me.zhengjie.modules.business.domain.WxUser;
import me.zhengjie.modules.business.service.dto.WxUserDto;
import me.zhengjie.modules.business.service.dto.WxUserQueryCriteria;
import org.springframework.data.domain.Pageable;
import org.springframework.web.multipart.MultipartFile;

import java.util.Map;
import java.util.List;
import java.io.IOException;
import javax.servlet.http.HttpServletResponse;

/**
* @author churuitao
* @date 2020-02-04
*/
public interface WxUserService {

    /**
    * 查询数据分页
    * @param criteria 条件
    * @param pageable 分页参数
    * @return Map<String,Object>
    */
    Map<String,Object> queryAll(WxUserQueryCriteria criteria, Pageable pageable);

    /**
    * 查询所有数据不分页
    * @param criteria 条件参数
    * @return List<WxUserDto>
    */
    List<WxUserDto> queryAll(WxUserQueryCriteria criteria);

    /**
     * 根据ID查询
     * @param id ID
     * @return WxUserDto
     */
    WxUserDto findById(Integer id);

    /**
    * 创建
    * @param resources /
    * @return WxUserDto
    */
    WxUserDto create(WxUser resources);

    /**
    * 编辑
    * @param resources /
    */
    void update(WxUser resources);

    /**
    * 多选删除
    * @param ids /
    */
    void deleteAll(Integer[] ids);

    /**
    * 导出数据
    * @param all 待导出的数据
    * @param response /
    * @throws IOException /
    */
    void download(List<WxUserDto> all, HttpServletResponse response) throws IOException;

    /**
     *
     * @description 判断认证状态
     * @param id id
     * @return
     * @author churuitao
     * @date 2020年02月05日 17:09
     */
    boolean checkAuthStatus(int id);

    /**
     *
     * @description 根据miniOpenid获取用户
     * @param openid openid
     * @return
     * @author churuitao
     * @date 2020年02月06日 16:14
     */
    WxUserDto findWxUserByMiniOpenid(String openid);



}