package me.zhengjie.modules.business.service;

import me.zhengjie.modules.business.domain.AuthData;
import me.zhengjie.modules.business.service.dto.AuthDataDto;
import me.zhengjie.modules.business.service.dto.AuthDataQueryCriteria;
import org.springframework.data.domain.Pageable;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;
import java.util.Map;

/**
* @author churuitao
* @date 2020-02-02
*/
public interface AuthDataService {

    /**
    * 查询数据分页
    * @param criteria 条件
    * @param pageable 分页参数
    * @return Map<String,Object>
    */
    Map<String,Object> queryAll(AuthDataQueryCriteria criteria, Pageable pageable);

    /**
    * 查询所有数据不分页
    * @param criteria 条件参数
    * @return List<AuthDataDto>
    */
    List<AuthDataDto> queryAll(AuthDataQueryCriteria criteria);

    /**
     * 根据ID查询
     * @param id ID
     * @return AuthDataDto
     */
    AuthDataDto findById(Integer id);

    /**
    * 创建
    * @param resources /
    * @return AuthDataDto
    */
    AuthDataDto create(AuthData resources);

    /**
    * 编辑
    * @param resources /
    */
    void update(AuthData resources);

    /**
    * 多选删除
    * @param ids /
    */
    void deleteAll(Integer[] ids);

    /**
    * 导出数据
    * @param all 待导出的数据
    * @param response /
    * @throws IOException /
    */
    void download(List<AuthDataDto> all, HttpServletResponse response) throws IOException;

    /**
     *  title 根据id查询认证资料
     *  Description 描述
     */
    AuthDataDto findByUserId(Integer id);

    /**
     *
     * description 创建修改认证资料同时关联用户
     * @param id
     * return
     * @author churuitao
     * date 2020年02月09日 9:00
     */
    void createOrUpdateByUserId(AuthData authData, Integer id);

    /**
     *
     * description 根据用户 id 查询用户认证状态
     * @param id
     * @return AuthDataStatusDto
     * @author churuitao
     * date 2020年02月09日 19:43
     */
    Map inquiryById(Integer id);

}