package me.zhengjie.modules.business.service.dto;

/**
 *  @title AccessToken
 *  @Description 描述
 *  @author churuitao
 *  @Date 2020年01月28日 14:11
 *  @Version: 1.0
 *  @Copyright 2020-2021 
 */
public class AccessToken {

    private String accessToken;
    private long expiresTime;

    public AccessToken(String accessToken, String expiresIn) {
        this.accessToken = accessToken;
        this.expiresTime = System.currentTimeMillis()+Integer.parseInt(expiresIn)*1000;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    public long getExpiresTime() {
        return expiresTime;
    }

    public void setExpiresTime(long expiresTime) {
        this.expiresTime = expiresTime;
    }

    /**
     *
     * @description 判断Token是否过期
     * @param null
     * @return
     * @author churuitao
     * @date 2020年01月28日 14:13
     */
    public boolean isExpired(){
        return System.currentTimeMillis()>this.expiresTime;
    }
}
